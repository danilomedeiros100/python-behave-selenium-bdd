from selenium.webdriver.common.by import By


class GooglePage:
    INPUT_SEARCH = (By.NAME, "q")
    BUTTON_SEARCH = (By.NAME, "btnK")
    TITLE_RESULT = (By.XPATH, "(//h3[contains(@class,'LC20lb MBeuO DKV0Md')])[1]")

    def __init__(self, driver):
        self.driver = driver

    def input_search(self, txt):
        self.driver.find_element(*self.INPUT_SEARCH).send_keys(txt)

    def btn_search(self):
        self.driver.implicitly_wait(10)
        self.driver.find_element(*self.BUTTON_SEARCH).click()

    def get_title_result(self):
        self.driver.implicitly_wait(10)
        el = self.driver.find_element(*self.TITLE_RESULT)
        return el.text
