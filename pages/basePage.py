
class BasePage:

    def __init__(self, driver):
        self.driver = driver

    def screenshot(self, path):
        self.driver.save_screenshot(path)
