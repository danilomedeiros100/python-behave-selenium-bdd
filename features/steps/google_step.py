from behave import given, when, then


@given(u'que acesso a página do Google')
def step_impl(context):
    context.driver.get("https://www.google.com.br")


@given(u'que preencho o campo de pesquisa com Python')
def step_impl(context):
    context.google.input_search("Python")


@when(u'clico no botão de pesquisar')
def step_impl(context):
    context.google.btn_search()


@then(u'devo visualizar os resultados')
def step_impl(context):
    assert context.google.get_title_result() == "Welcome to Python.org"


# _____________________ Cenário Outline

@given(u'que preencho o campo de pesquisa com "{index}"')
def step_impl(context, index):
    context.google.input_search(index)


@then(u'devo visualizar os resultados "{resultado}"')
def step_impl(context, resultado):
    assert context.google.get_title_result() == resultado


