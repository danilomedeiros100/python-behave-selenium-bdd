import os
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from pages.basePage import BasePage
from pages.google_page import GooglePage


def before_all(context):
    options = webdriver.ChromeOptions()
    service = Service("driver/chromedriver")
    options.add_argument("start-maximized")
    context.driver = webdriver.Chrome(service=service, options=options)
    context.google = GooglePage(context.driver)
    context.base = BasePage(context.driver)


def before_feature(context, feature):
    context.nomeFeature = feature.name.replace(" ", "_")
    if os.path.isdir("./screenshot/" + context.nomeFeature):
        pass
    else:
        os.mkdir("./screenshot/" + context.nomeFeature)


def before_scenario(context, scenario):
    context.nomeScenario = scenario.name.replace(" ", "_")


def after_step(context, step):
    context.stepStatus = step.status
    if step.status.value == 2:
        context.stepStatus = "passed"
    else:
        context.stepStatus = "failed"
    context.nomeStep = step.name.replace(" ", "_")

    if os.path.isdir("./screenshot/" + context.nomeFeature + "/" + context.nomeScenario):
        pass
    else:
        os.mkdir("./screenshot/" + context.nomeFeature + "/" + context.nomeScenario)

    context.base.screenshot(
        "screenshot/" + context.nomeFeature + "/" + context.nomeScenario + "/" + context.stepStatus + "_" + context.nomeStep + ".png")
