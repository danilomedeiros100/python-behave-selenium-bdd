

  Feature: Google validate


  Background: acessar página de teste
    Given que acesso a página do Google

  Scenario: realizar pesquisa no google pela palavra python
    Given que preencho o campo de pesquisa com Python
    When clico no botão de pesquisar
    Then devo visualizar os resultados


 Scenario Outline: realizar pesquisa no google por diversos termos
    Given que preencho o campo de pesquisa com "<index>"
    When clico no botão de pesquisar
    Then devo visualizar os resultados "<resultado>"

   Examples:
     | index    | resultado             |
     | python   | Welcome to Python.org |
     | selenium | Selenium              |