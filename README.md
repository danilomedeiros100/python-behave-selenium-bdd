# python-behave-selenium-bdd

## Pré-requisitos

    $ Ter a versão do python3 instalado;

Cheque a versão do pyhton que está instalado com:
	
	python3 --version


## Como usar

1. Clone o repositório em sua máquina.
2. Navegue até a pasta do projeto.
3. Execute o seguinte comando para executar todos os testes:

### Virtualenv

O virtualenv permite ter um ambiente python isolado para o projeto. É fortemente recomendável o seu uso em máquina local. 

Em caso do MacOs:

    virtualenv -p python3 venv
    . ./venv/bin/activate

Em caso de Windows:

    virtualenv -p python3 venv
    . ./venv/Scripts/activate

### Instalando dependências
Para realizar a instalação das dependências basta executar o comando após entrar na sua máquina virtual:

    pip install -r requirements.txt

### Executando
Para executar todos os cenários:

    behave